class SkillsController < ApplicationController

    
    def create 
      @skill = Skill.find(params[:select_tag])
      
      @user = current_user
      @user.skills << @skill
      
      #@skill.save!
      
      respond_to do |format|
        format.html { redirect_to edit_user_registration_path }
        format.js
      end
  
    end

    

    def add_skill_params
        params.require(:skill).permit(:name)
    end

    def show
      @skill = Skill.find(params[:id])
    end
    
    def destroy
      @skill = Skill.find(params[:id])

      current_user.skills.destroy(params[:id])

      respond_to do |format|
        format.html { redirect_to edit_user_registration_path }
        format.js
      end
    end


    def add_skill
      @skill = Skill.find(params[:id])
      
      ActiveRecord::Base.connection.execute("INSERT INTO skills_users ('skill_id', 'user_id') VALUES ('%#{@skill.id}%', '%#{current_user.id}%')")
      
      @skill.save!
      
      respond_to do |format|
        format.html { redirect_to edit_user_registration_path }
        format.js
      end

    end

end
