class PagesController < ApplicationController
  def home
  end

  def events
  end

  def talent
    @users = User.paginate(:page => params[:page], :per_page => 10)
  end

  def leadership 
  end

  def mailcheck
  end

end
