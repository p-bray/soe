class UsersController < ApplicationController
  
  def show
    @user = User.find(params[:id])
    @skills = @user.skills
  end

  def update 
    @user = User.find(current_user.id)

    if @user.update_attributes(update_user_params)
      render "pages/home"
    else
      render 'edit'
    end
  end

  def update_user_params
    params.require(:user).permit(:name, :description, :area_code, :prefix, :line_number)
  end

  def index 
    if params[:search].blank?
      @users = User.order(:name) 
    else
      @users = User.search(params[:search])
      params.delete :search
    end
  end
end


