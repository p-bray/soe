Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: "registrations", passwords: 'passwords', confirmations: 'confirmations' }
  default_url_options :host => "www.paytonbray.com"
  
  resources :users
  resources :skills
  
  get 'pages/home'
  get 'pages/events'
  get 'pages/talent'
  get 'pages/leadership'
  get 'pages/mailcheck'
  post 'users/add_skill'
  root 'pages#home'
end