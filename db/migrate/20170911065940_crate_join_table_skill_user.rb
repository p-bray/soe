class CrateJoinTableSkillUser < ActiveRecord::Migration[5.1]
  def change
    create_join_table :skills, :users do |t|
      t.belongs_to :skill, index: true
      t.belongs_to :user, index: true
    end
  end
end
