# README

* Ruby version: ruby 2.4.1p111 (2017-03-22 revision 58053)

* System dependencies: nil

* Configuration

* Database creation
    -install postgresql here: https://www.postgresql.org/download/
    -add your personal password and whatever other unique connection settings you have to config/database.yml
    -run 'rake db:create' in the terminal in the project directory

* Database initialization
    -run 'rake db:migrate' in the terminal in the project directory

* How to run the test suite
    -no test suite currently

* Services (job queues, cache servers, search engines, etc.)
    -no services currently. 

* Deployment instructions
    -no deployment location currently. 
* ...
